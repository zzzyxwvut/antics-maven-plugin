A handful of Apache Ant tasks: `touch`, `native2ascii`.

Make a local installation and learn about available goals:  
`mvn install`  
`mvn help:describe -Dplugin=org.zzzyxwvut:antics-maven-plugin -Ddetail=true`
